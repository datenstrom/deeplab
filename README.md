## DeepLab

Setup for ML versioning, tracking, etc. server.

### Setup Details

- **MLFlow:** is installed in a conda environment `mlflow`
  - **PostgreSQL:** `postgresql://mlflow:mlflow@localhost/mlflow` port `8000`
  - Storage root location: `/media/storage/mlflow`
  - Service logs: `/var/log/mlflow-err.log`, `/var/log/mlflow-out.log`
  - Artifact storage: `/media/storage/mlflow/artifacts`
  - Metadata storage: `/media/storage/mlflow/postgresql/10/main`
- **DVC**
  - Storage root: `/media/storage/dvc`
  - Access group: `dvc`


#### PostgreSQL

```bash
sudo chown root:postgres /var/log/postgresql
sudo chmod g+wx /var/log/postgresql
sudo -u postgres /usr/lib/postgresql/11/bin/initdb /my/data/location
```

#### DVC

DVC users must be added to the `dvc` group and given SSH access.

#### ZFS

If using ZFS on Debian `contrib` must be added to `/etc/apt/sources.list`
sources as well as `buster-backports`. 

See [ZFS on Linux FAQ](https://github.com/zfsonlinux/zfs/wiki/faq#performance-considerations)
for more information such as:

- [ArchWiki's excellent docs](https://wiki.archlinux.org/index.php/ZFS)
- [Selecting `/dev/` names for a pool](https://github.com/zfsonlinux/zfs/wiki/faq#selecting-dev-names-when-creating-a-pool)
- [performance considerations](https://github.com/zfsonlinux/zfs/wiki/faq#performance-considerations)
- [Advanced Format Disks](https://github.com/zfsonlinux/zfs/wiki/faq#advanced-format-disks)
- [ECC memory](https://github.com/zfsonlinux/zfs/wiki/faq#do-i-have-to-use-ecc-memory-for-zfs)
- [`/etc/zfs/zpool.cache`](https://github.com/zfsonlinux/zfs/wiki/faq#the-etczfszpoolcache-file)

Note that `ashift` of exisitng pools can be checked using:

- `zdb -C | grep ashift`
- `zdb -U /etc/zfs/zpool.cache`
