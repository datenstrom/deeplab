#!/bin/env sh
#
# Useful functions portable across bash and zsh.

# Easy to use base colors for scripts.
#
# See logging functions below for examples.
#
# Only runs `tput` if session is interactive and
# TTY is assigned falling back to non-colored
# output seamlessly where it is not supported.
_colors()
{
    if test -t 1; then
        RED=$(tput setaf 1)
        YELLOW=$(tput setaf 3)
        CYAN=$(tput setaf 6)
        NORMAL=$(tput sgr0)
    else
        RED=''
        YELLOW=''
        CYAN=''
        NORMAL=''
    fi
}

# Print a red error message.
_error()
{
    _colors
    printf "${RED}[*] Error: ${NORMAL}$1"
    echo ""
}

# Print a yellow warning message.
_warning()
{
    _colors
    printf "${YELLOW}[*] Warning: ${NORMAL}$1"
    echo ""
}

# Print a cyan info message.
_info()
{
    _colors
    printf "${CYAN}[*] Info: ${NORMAL}$1"
    echo ""
}

# Print a horizontal rule the exact width of the term.
#
# Only executes if session is interactive and
# TTY is assigned has no effect where it is
# not supported.
_rule()
{
    if test -t 1; then
        printf -v _hr "%*s" $(tput cols) && echo ${_hr// /${1--}}
    fi
}

# Test if a command exists.
#
# Returns 0 if command exists, otherwise returns 1.
_command_exists()
{
    command -v "$@" > /dev/null 2>&1
}

# Ask user a yes or no question.
#
# Prefers using a graphical dialog box and falls back to
# text prompt if no supported command is found. Will
# continually prompt user until an acceptable answer is
# provided.
#
# In general, POSIX is very consistent, and in all cases
# where the return value is only used to signal success or
# failure, 0 means success.
#
# See the POSIX function definition for more information:
#   * http://www.unix.com/man-page/all/3posix/pthread_mutex_trylock/
_ask_yes_no()
{
    if $(_command_exists whiptail); then
        dialog_box="whiptail"
    elif $(_command_exists dialog); then
        dialog_box="dialog"
    elif $(_command_exists gdialog); then
        dialog_box="gdialog"
    elif $(_command_exists kdialog); then
        dialog_box="kdialog"
    else
        dialog_box=""
    fi

        dialog_box=""

    if [[ ! -z $dialog_box ]]; then
        sh -c "$dialog_box --yesno \"$1\" 0 0" 
        result="$?"
    else
        while true; do
            echo -n "$1? (y/n) "
            read answer
            case "$answer" in
                y | Y | yes | YES)
                    result=0
                    break
                    ;;
                n | N | no | NO)
                    result=1
                    break
                    ;;
            esac
        done
    fi

    return "$result"
}
