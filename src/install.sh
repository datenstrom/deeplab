#!/bin/bash

set -e

source posix_common.sh

#bash miniconda.sh

_info "Installing MLFlow environment"
#export PATH="/opt/miniconda/bin:$PATH"
#sudo /opt/miniconda/bin/conda create -y --prefix /opt/miniconda/envs/mlflow
#conda init bash
#source ~/.bashrc
#conda activate /opt/miniconda/envs/mlflow
sudo apt-get install -y python3-pip
sudo pip3 install mlflow

bash postgres.sh
sudo mkdir -p /media/storage/mlflow/artifacts

_info "Starting MLFlow service"
sudo cp mlflow-tracking.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable mlflow-tracking
sudo systemctl start mlflow-tracking

bash dvc.sh

_info "DVC users must be added to the 'dvc' group"
_info "Setup complete"
