#!/bin/bash

set -e

source posix_common.sh

_info "Installing PostgreSQL"

sudo apt-get install -y postgresql postgresql-contrib postgresql-server-dev-all

sudo mkdir -p /media/storage/mlflow/postgresql/11/main
sudo sed -i "s|'/var/lib/postgresql/11/main'|'/media/storage/mlflow/postgresql/11/main'|g" /etc/postgresql/11/main/postgresql.conf

while true; do
    read -s -p 'Enter password for `mlflow` PostgreSQL user: ' PASSWORD
    echo
    read -s -p 'Password (confirm): ' PASSWORD2
    echo
    [ "$PASSWORD" = "$PASSWORD2" ] && break
    echo "Please try again"
done

id -u postgres &>/dev/null || useradd --disabled-password --gecos "" postgres
sudo -u postgres psql -U postgres postgres <<OMG
    CREATE DATABASE mlflow;
    CREATE USER mlflow WITH ENCRYPTED PASSWORD "$PASSWORD";
    GRANT ALL PRIVILEGES ON DATABASE mlflow TO mlflow;
OMG

# For MLFlow interaction
sudo apt-get install gcc
sudo pip3 install psycopg2
