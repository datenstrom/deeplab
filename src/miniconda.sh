#!/bin/bash

set -e

source posix_common.sh

_info "Installing Miniconda"

sudo apt-get install -y curl
curl -sSL https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o miniconda_install.sh
sudo bash miniconda_install.sh -b -f -p "/opt/miniconda" || exit 1
rm miniconda_install.sh
