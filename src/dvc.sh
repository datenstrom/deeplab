#!/bin/bash

set -e

# Group for users
sudo groupadd dvc
sudo mkdir -p /media/storage/dvc
sudo chown -R root:dvc /media/storage/dvc
