#!/bin/bash

# NOTE:
#
# ZFS on Linux is provided in the form of DKMS source for Debian users,
# you would need to add contrib section to your apt sources configuration
# to be able to get the packages. Also, it is recommended by Debian ZFS on
# Linux Team to install ZFS related packages from Backports archive,
# upstream stable patches will be tracked and compatibility is always
# maintained.

set -e

sudo apt-get update

# The steps of installing Linux headers, spl and zfs. It's fine to combine
# everything in one command but let's be explict to avoid any chance of
# messing up with versions, future updates will be taken care by apt. 
sudo apt-get install -y linux-headers-`uname -r`
sudo apt-get install -y -t buster-backports dkms spl-dkms
sudo apt-get install -y -t buster-backports zfs-dkms zfsutils-linux
